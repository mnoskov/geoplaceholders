/**
 * prepareRelated
 *
 * Данный сниппет нужно добавить в параметры DocLister, если нужно выводить с учетом значений related_to
 *
 * @category    snippet
 * @internal    @overwrite true
*/
//<?php
$relate = $_extDocLister->getStore('relate');

if (is_null($relate)) {
    $tvid = $modx->db->getValue($modx->db->select('id', $modx->getFullTablename('site_tmplvars'), "`name` = 'related_to'"));

    $relate = [
        'relations' => [],
        'templates' => [],
    ];

    if (!empty($tvid)) {
        $templates = $modx->db->getColumn('templateid', $modx->db->select('templateid', $modx->getFullTablename('site_tmplvar_templates'), "`tmplvarid` = '$tvid'"));
        $relate['templates'] = array_flip($templates);

        $query = $modx->db->select('*', $modx->getFullTablename('site_tmplvar_contentvalues'), "`tmplvarid` = '$tvid'");

        while ($row = $modx->db->getRow($query)) {
            $values = explode('||', trim($row['value'], '|'));

            if (!empty($values)) {
                $relate['relations'][$row['contentid']] = $values;
            }
        }
    }

    $_extDocLister->setStore('relate', $relate);
}

if (!empty($modx->geo->location['city_id']) && isset($relate['templates'][$data['template']])) {
    if (!isset($relate['relations'][$data['id']])) {
        return false;
    }

    if (isset($relate['relations'][$data['id']]) && !in_array($modx->geo->location['city_id'], $relate['relations'][$data['id']])) {
        return false;
    }
}

return $data;
