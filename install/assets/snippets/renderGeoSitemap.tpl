/**
 * renderGeoSitemap
 *
 * Формирует карту сайта для всех гео-подразделов, с учетом установленных связей
 *
 * @category    snippet
 * @internal    @overwrite true
*/
//<?php

$root = isset($root) ? $root : 1;

$children = $modx->getChildIds($root);
$docs = $modx->getDocuments($children);

$tvid = $modx->db->getValue($modx->db->select('id', $modx->getFullTablename('site_tmplvars'), "`name` = 'related_to'"));
$relations = $tempaltes = [];

if (!empty($tvid)) {
    $templates = $modx->db->getColumn('templateid', $modx->db->select('templateid', $modx->getFullTablename('site_tmplvar_templates'), "`tmplvarid` = '$tvid'"));
    $templates = array_flip($templates);

    $query = $modx->db->select('*', $modx->getFullTablename('site_tmplvar_contentvalues'), "`tmplvarid` = '$tvid'");

    while ($row = $modx->db->getRow($query)) {
        $values = explode('||', trim($row['value'], '|'));

        if (!empty($values)) {
            $relations[$row['contentid']] = $values;
        }
    }
}

$city = $modx->geo->location['alias'];

$xml = new DOMDocument('1.0', 'UTF-8');
$xml->formatOutput = true;
$set = $xml->appendChild($xml->createElement('urlset'));
$set->setAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');

foreach ($modx->geo->records as $location) {
    foreach ($docs as $doc) {
        if (isset($templates[$doc['template']])) {
            if (!isset($relations[$doc['id']])) {
                continue;
            }

            if (isset($relations[$doc['id']]) && !in_array($location['city_id'], $relations[$doc['id']])) {
                continue;
            }
        }

        $link = $modx->makeUrl($doc['id'], '', '', 'full');
        if ($city != $location['alias']) {
            $link = str_replace('/' . $city . '/', '/' . $location['alias'] . '/', $link);
        }

        $url = $xml->createElement('url');
        $url->appendChild($xml->createElement('loc', $link));
        $url->appendChild($xml->createElement('lastmod', (new DateTime())->setTimestamp($doc['editedon'])->format('Y-m-d')));
        $url->appendChild($xml->createElement('changefreq', 'monthly'));
        $url->appendChild($xml->createElement('priority', 0.8));
        $set->appendChild($url);
    }
}

return $xml->saveXML();
