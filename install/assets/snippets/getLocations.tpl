/**
 * getLocations
 *
 * getLocations
 *
 * @category    snippet
 * @internal    @overwrite true
*/
//<?php

if (empty($modx->geo->records)) {
    return '';
}

if ($for == 'dropdown') {
    $out = [];

    foreach ($modx->geo->records as $location) {
        $out[] = [$location['title'], $location['city_id']];
    }
} else {
    $url = $_GET;
    $q = $url['q'];
    $q = explode('/', $q);
    if ($q[0] == $location['alias']) {
        array_shift($q);
    }
    $q = implode('/', $q);
    unset( $url['q'] );
    unset( $url['region'] );
    unset( $url['city'] );
    $params = http_build_query( $url );
    $url = '/' . $q . ( !empty( $params ) ? '?' . $params : '' );

    if ($for == 'modal-right') {
        $locations = $modx->geo->records;
    } else {
        $locations = array_filter($modx->geo->records, function($item) {
            return !empty($item['is_master']);
        });
    }

    $out = '';

    require_once MODX_BASE_PATH . 'assets/snippets/DocLister/lib/DLTemplate.class.php';
    $DLTemplate = DLTemplate::getInstance($modx);

    if (empty($tpl)) {
        $tpl = '@CODE:<li><a href="[+url+]"[+class+]>[+title+]</a></li>';
    }

    if (empty($ownerTPL)) {
        $ownerTPL = '@CODE:<ul class="cities">[+wrap+]</ul>';
    }

    foreach ($locations as $location) {
        $class = $location['alias'] == $modx->geo->location['alias'] ? 'current' : '';

        $out .= $DLTemplate->parseChunk($tpl, array_merge($location, [
            'url'       => $location['alias'] . $url,
            'class'     => !empty($class) ? ' class="' . $class . '"' : '',
            'classname' => $class,
        ]));
    }

    if (!empty($out)) {
        $out = $DLTemplate->parseChunk($ownerTPL, ['wrap' => $out]);
    }

    return $out;
}

return $out;
