//<?php
/**
 * Install
 *
 * Plugin installer
 *
 * @category    plugin
 * @author      mnoskov
 * @internal    @events OnWebPageInit,OnManagerPageInit,OnPageNotFound
 * @internal    @installset base
*/

$modx->clearCache('full');

$pagebuilder_data = [
    [
        'document_id' => 0,
        'container'   => 'locations',
        'title'       => '',
        'visible'     => 1,
        'index'       => 0,
        'config'      => 'locations',
        'values'      => $modx->db->escape(json_encode([
            'title'     => 'Пермь',
            'title2'    => 'Перми',
            'alias'     => 'perm',
            'region_id' => '511180',
            'city_id'   => '511196',
            'cities'    => [],
        ], JSON_UNESCAPED_UNICODE)),
    ],
];

$pagebuilder = $modx->getFullTablename('pagebuilder');

foreach ($pagebuilder_data as $row) {
    $modx->db->insert($row, $pagebuilder);
}

$system_settings = $modx->getFullTablename('system_settings');

$settings = [
    'client_default_location' => '511196',
];

foreach ($settings as $key => $value) {
    $modx->db->save([
        'setting_name'  => $key,
        'setting_value' => $value,
    ], $system_settings, "`setting_name` = '$key'");
}


// remove installer
$site_plugins = $modx->getFullTablename('site_plugins');
$site_plugin_events = $modx->getFullTablename('site_plugin_events');

$query = $modx->db->select('id', $site_plugins, "`name` = 'Install'");

if ($id = $modx->db->getValue($query)) {
   $modx->db->delete($site_plugins, "`id` = '$id'");
   $modx->db->delete($site_plugin_events, "`pluginid` = '$id'");
};
