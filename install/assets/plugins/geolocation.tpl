//<?php
/**
 * geolocation
 *
 * geolocation
 *
 * @category    plugins
 * @internal    @events OnWebPageInit,OnManagerPageInit,OnPageNotFound,OnMakeDocUrl
 */

require_once MODX_BASE_PATH . 'assets/plugins/geolocation/geolocation.php';

$e = &$modx->event;

switch($e->name) {
    case 'OnWebPageInit':
    case 'OnManagerPageInit':
    case 'OnPageNotFound': {
        $modx->geo = new Geolocation($modx, $params);

        // Для админки дальнейшую проверку не делаем
        if ($e->name == 'OnManagerPageInit') {
            break;
        }

        foreach ($modx->geo->location as $field => $value) {
            $modx->setPlaceholder('geo_' . $field, $value);
        }

        foreach (['address', 'phone', 'email'] as $field) {
            if (isset($modx->geo->location[$field])) {
                $modx->config['client_company_' . $field] = $modx->geo->location[$field];
            }
        }

        // Параметр $_GET['city'] берется из .htaccess rewrite rules
        // и равен названию первого каталога из url.
        // Если мы находим город с таким же названием, то заканчиваем.
        if (isset($_GET['city'])) {
            if ($modx->geo->location['alias'] == $_GET['city']) {
                break;
            }

            if (in_array($_GET['city'], array_column($modx->geo->records, 'alias'))) {
                break;
            }
        }

        if (preg_match('/^\/(ajax\.json|sitemap\.xml|custom|commerce\/)/', $_SERVER['REQUEST_URI'])) {
            break;
        }

        // Если же нет, добавляем в начало url название города и переходим к нему
        // (это будет означать, что название первого каталога не является городом).
        $modx->sendRedirect('/' . $modx->geo->location['alias'] . $_SERVER['REQUEST_URI']);
    }

    case 'OnMakeDocUrl': {
        if (!empty($modx->geo->location)) {
            $host = rtrim($modx->getConfig('site_url'), '/');
            $url  = ltrim($e->params['url'], '/');

            if (strpos($url, $host) !== false) {
                $url = str_replace($host, '', $url);
                $url = $host . '/' . $modx->geo->location['alias'] . '/' . ltrim($url, '/');
            } else {
                $url = '/' . $modx->geo->location['alias'] . '/' . $url;
            }

            $e->output($url);
        }

        break;
    }
}
