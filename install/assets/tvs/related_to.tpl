﻿/**
 * related_to
 *
 * Связь ресурсов с геообъектами
 *
 * @category        tv
 * @name            related_to
 * @internal        @caption Связь ресурсов с геообъектами
 * @internal        @input_type checkbox
 * @internal        @input_options @EVAL return $modx->runSnippet('getLocations', ['for' => 'dropdown']);
 * @internal        @input_default 0
 * @internal        @output_widget 
 * @internal        @output_widget_params 
 * @internal        @lock_tv 0
 */