<?php

require_once 'sxgeo.php';

class Geolocation
{
    public $location;

    private $modx;
    private $geo      = null;
    private $ip       = null;
    private $fallback = null;
    private $cookie   = 'geodata_bM6RPvGAAqQnvEUHMpc6';
    private $datafile = 'assets/plugins/geolocation/SxGeoCity.dat';
    private $url      = 'http://ru3.sxgeo.city';

    public function __construct($modx, $params = [])
    {
        $this->modx = $modx;

        foreach (['cookie', 'datafile', 'url'] as $field) {
            if (!empty($params[$field])) {
                $this->{$field} = $params[$field];
            }
        }

        $data = $modx->runSnippet('PageBuilder', [
            'docid'     => 0, 
            'container' => 'locations', 
            'tv'        => true, 
            'renderTo'  => 'array',
        ]);

        $this->records = [];

        foreach (array_shift($data) as $row) {
            $master = array_diff_key($row, ['cities' => 1]);
            $this->records[$master['city_id']] = $master;
            $this->records[$master['city_id']]['is_master'] = 1;

            foreach ($row['cities'] as $slave) {
                $this->records[$slave['city_id']] = array_merge($master, $slave);
            }
        }

        uasort($this->records, function($a, $b) {
            return $a['title'] > $b['title'];
        });

        $location = $this->locate();

        if (isset($this->records[$location['city']])) {
            $this->location = $this->records[$location['city']];
        } else {
            if ($row = $this->filter(['region_id' => $_GET['region']])) {
                $this->location = $row;
            } else {
                $default = $this->modx->getConfig('client_default_location');
                
                if (isset($this->records[$default])) {
                    $this->location = $this->records[$default];
                } else {
                    $this->location = reset($this->records);
                }
            }
        }
    }

    private function setcookie($data)
    {
        setcookie($this->cookie, base64_encode(json_encode($data)), time() + 2592000, '/');
    }

    private function fallback()
    {
        if ($this->fallback !== null) {
            return $this->fallback;
        }

        $row = reset($this->records);

        $this->fallback = [
            'city'   => $row['city_id'], 
            'region' => $row['region_id'], 
            'manual' => true,
        ];

        return $this->fallback;
    }

    private function ip()
    {
        if (!$this->ip) {
            foreach (['HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'REMOTE_ADDR', 'SERVER_ADDR'] as $field) {
                if (!empty($_SERVER[$field]) && strpos($_SERVER[$field], ',') === FALSE) {
                    $this->ip = $_SERVER[$field];
                    break;
                }
            }
        }

        return $this->ip;
    }

    public function filter($filter) {
        return reset(array_filter($this->records, function($item) use ($filter) {
            foreach ($filter as $field => $value) {
                if ($item[$field] != $value) {
                    return false;
                }
            }

            return true;
        }));
    }

    private function locate()
    {
        if (isset($_GET['city'])) { 
//echo 'from param<br>';
            $alias = $_GET['city'];

            if ($row = $this->filter(['alias' => $_GET['city']])) {
                $result = [
                    'city'   => $row['city_id'],
                    'region' => $row['region_id'],
                    'manual' => true,
                ];

                $this->setcookie($result);
                return $result;
            }
        }

        if (isset($_COOKIE[$this->cookie])) {
//echo 'from cookie<br>';
            $cookie = json_decode(base64_decode($_COOKIE[$this->cookie]), true);

            if ($cookie !== null) {
                $result = [
                    'city'   => isset($cookie['city']) && is_numeric($cookie['city']) ? intval($cookie['city']) : 0, 
                    'region' => isset($cookie['region']) && is_numeric($cookie['region']) ? intval($cookie['region']) : 0, 
                    'manual' => true,
                ];

                return $result;
            }
        }

        if (file_exists(MODX_BASE_PATH . $this->datafile)) {
//echo 'from file<br>';
            $api = new SxGeo(MODX_BASE_PATH . $this->datafile);
            $geo = $api->getCityFull($this->ip());
        }

        if (empty($geo)) {
            $isBot = isset($_SERVER['HTTP_USER_AGENT']) && preg_match('/(Google|Yahoo|Rambler|Bot|Yandex|Spider|Snoopy|Crawler|Finder|Mail|curl)/i', $_SERVER['HTTP_USER_AGENT']);

            if (!$isBot) {
//echo 'from api<br>';
                $geo = @file_get_contents($this->url . '/json/' . $this->ip());
                $geo = json_decode($geo, true);
            }
        }

        if (!empty($geo)) {
            $result = [
                'manual' => false,
            ];

            if (!empty($geo['region']['id'])) {
                $result['region'] = $geo['region']['id'];
            }

            if (!empty($geo['city']['id'])) {
                $result['city'] = $geo['city']['id'];
            }
        }

        if (empty($result)) {
            $result = $this->fallback();
        }

        $this->setcookie($result);
        return $result;
    }
}
