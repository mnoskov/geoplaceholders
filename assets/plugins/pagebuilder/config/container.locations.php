<?php

    return [
        'title' => 'Settings',

        'show_in_docs' => [ 0 ],

        'placement' => 'tv',

        'templates' => [
            'owner' => '
                <div>
                    Container:
                    [+wrap+]
                </div>
            ',
        ],
    ];

