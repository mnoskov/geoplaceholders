<?php

return [
    'title' => 'Города',

    'show_in_docs' => [ 0 ],

    'container' => 'locations',

    'templates' => [
        'owner' => '
            <div>Text:<br> [+text+]</div>
            <div>Images:<br> [+images+]</div>
        ',

        
    ],

    'fields' => [
        'title' => [
            'caption' => 'Название',
            'type'    => 'text',
            'note'    => '[&plus;geo_title+]',
        ],

        'alias' => [
            'caption' => 'URL псевдоним',
            'type'    => 'text',
            'note'    => '[&plus;geo_alias+]',
        ],

        'address' => [
            'caption' => 'Адрес',
            'type'    => 'text',
            'note'    => '[&plus;geo_address+]',
        ],

        'short_address' => [
            'caption' => 'Короткий адрес',
            'type'    => 'text',
            'note'    => '[&plus;geo_short_address+]',
        ],

        'phone' => [
            'caption' => 'Телефон',
            'type'    => 'text',
            'note'    => '[&plus;geo_phone+]',
        ],

        'email' => [
            'caption' => 'Email',
            'type'    => 'text',
            'note'    => '[&plus;geo_email+]',
        ],

        'title2' => [
            'caption' => 'Название в предложном падеже',
            'type'    => 'text',
            'note'    => 'О ком, о чем, [&plus;geo_title2+]',
        ],

        'region_id' => [
            'caption' => 'Идентификатор региона',
            'type' => 'text',
        ],

        'city_id' => [
            'caption' => 'Идентификатор города',
            'type' => 'text',
        ],

        'cities' => [
            'caption' => 'Города, использующие эти контактные данные',
            'type'    => 'group',
            'layout'  => 'horizontal',
            'fields'  => [
                'title' => [
                    'caption' => 'Название',
                    'type'    => 'text',
                ],

                'title2' => [
                    'caption' => 'Название в предложном падеже',
                    'type'    => 'text',
                ],

                'alias' => [
                    'caption' => 'URL псевдоним',
                    'type'    => 'text',
                ],

                'region_id' => [
                    'caption' => 'Идентификатор региона',
                    'type' => 'text',
                ],

                'city_id' => [
                    'caption' => 'Идентификатор города',
                    'type' => 'text',
                ],
            ],
        ],
    ],
];

