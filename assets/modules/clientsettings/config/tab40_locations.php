<?php

return [
    'caption' => 'Города',
    'settings' => [
        'default_location' => [
            'caption' => 'Город по умолчанию',
            'type' => 'dropdown',
            'elements' => '@EVAL return $modx->runSnippet("getLocations", ["for" => "dropdown"]);',
        ],
        'locations' => [
            'caption' => 'Города',
            'type' => 'custom_tv:pagebuilder',
        ],
    ],
];
